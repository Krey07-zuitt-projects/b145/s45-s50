const coursesData = [
	{
		id: "wdc0001",
		name: "PHP-LARAVEL",
		description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem sint sit officiis quo in doloremque neque qui voluptatum asperiores repudiandae impedit, cupiditate ducimus dicta accusantium assumenda aliquid atque molestiae quia.",
		price: 45000,
		onOffer: true
	},

	{
		id: "wdc0002",
		name: "Python - Django",
		description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem sint sit officiis quo in doloremque neque qui voluptatum asperiores repudiandae impedit, cupiditate ducimus dicta accusantium assumenda aliquid atque molestiae quia.",
		price: 50000,
		onOffer: true
	},

	{
		id: "wdc0003",
		name: "Java - Springboot",
		description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem sint sit officiis quo in doloremque neque qui voluptatum asperiores repudiandae impedit, cupiditate ducimus dicta accusantium assumenda aliquid atque molestiae quia.",
		price: 55000,
		onOffer: true
	},

]

export default coursesData;