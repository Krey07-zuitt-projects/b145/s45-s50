import { Link } from "react-router-dom";
import { Row, Col, Button } from "react-bootstrap";

export default function ErrorPage() {
	return (
		<Row>
			<Col className="p-7 text-center">
				<h1>Page Not Found</h1>
				<p>
					Go back to the <Link to="/">homepage</Link>
				</p>
			</Col>
		</Row>
	);
}
