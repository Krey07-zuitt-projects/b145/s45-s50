import React from 'react'
import { Navigate, Link } from 'react-router-dom'
import { Fragment } from 'react'
import { Nav } from 'react-bootstrap'
export default function Error() {
	return (
		<Fragment>
			<h2 className="mt-3">Error 404: Page not found!</h2>
			<Nav.Link as= { Link } to= "/">Home</Nav.Link>
		</Fragment>
	)
}
