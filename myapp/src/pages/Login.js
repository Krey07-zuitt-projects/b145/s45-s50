import { Form, Button} from 'react-bootstrap'
import { useState, useEffect, useContext, Fragment } from 'react'
import { Navigate } from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Login() {

	const { user, setUser } = useContext(UserContext)

	const [email, loginEmail] = useState('')
  	const [password, loginPassword] = useState('')
  	const [isActive, setIsActive] = useState(false)

  	useEffect(() => {
		if(email.length > 0 && password.length > 0) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password])

  	function loginUser(e) {
		e.preventDefault()

		//Syntax: fetch(url, {options})
		//.then(res => res.json())
		//.then(data => {})

		fetch ('https://infinite-wave-53290.herokuapp.com/users/signin', 
			{
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					email: email,
					password: password
				})
			}
		)
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if (typeof data.access !== 'undefined') {
				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)

				Swal.fire({
					title: 'Login Successful',
					icon: 'success',
					text: 'Welcome to Zuitt'
				})
			} else {
				Swal.fire({
					title: 'Wrong username/password',
					icor: 'error',
					text: 'Please try again'
				})
			}
		})

		// localStorage.setItem('email', email)
		//localStorage.setItem('key', value)
		// setUser({
		// 	email: localStorage.getItem('email')
		// })

		// Clear input fields
		loginEmail('')
		loginPassword('')

		// alert('You are now logged in!')
	}

	const retrieveUserDetails = (token) => {
		fetch ('https://infinite-wave-53290.herokuapp.com/users/getuser', {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	return (
	(user.id !== null) ?
		<Navigate to="/courses"/>
	:
		<Fragment>
		<h4 className="mt-3">Login</h4>
		<Form onSubmit={(e) => loginUser(e)}>
			<Form.Group controlId="email">
				<Form.Label>Email Address:</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email"
					value={email}
					onChange={e => loginEmail(e.target.value)}
					required

				/>
			</Form.Group>
			<Form.Group controlId="password">
				<Form.Label className="mt-3">Password:</Form.Label>
				<Form.Control 
					type="password"
					placeholder="Password"
					value={password}
					onChange={e => loginPassword(e.target.value)}
					required
				/>
			</Form.Group>
		{ isActive ?
		<Button variant="success" type="submit" id="submitBtn" className="mt-3">
			Login
		</Button>

		:
		<Button variant="danger" type="submit" id="submitBtn" className="mt-4" disabled>
			Login
		</Button>
		}
		</Form>
		</Fragment>
	)
}
